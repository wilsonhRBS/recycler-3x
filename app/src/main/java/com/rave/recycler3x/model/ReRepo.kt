package com.rave.recycler3x.model

import android.graphics.Color
import com.rave.recycler3x.api_delay
import com.rave.recycler3x.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object ReRepo {
    private val repo: ReApi = object: ReApi {
        override fun getRandomColor(): Int {
            return Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        }
    }

    suspend fun fetchColorList(len: Int): Resource = withContext(Dispatchers.IO) {
        delay(api_delay)
        val colorList = IntArray(len) { repo.getRandomColor() }.asList()
        return@withContext Resource.Success(colorList)
    }
}