package com.rave.recycler3x.util

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rave.recycler3x.databinding.SelectionHorizontalRecyclerBinding

class SelectionVerticalAdapter: RecyclerView.Adapter<SelectionVerticalAdapter.RecyclerViewHolder>() {
    private var vertColorList = mutableListOf<List<Int>>()

    class RecyclerViewHolder(private val binding: SelectionHorizontalRecyclerBinding): RecyclerView.ViewHolder(binding.root) {
        fun add(colors: List<Int>) {
            val horizontalAdapter = SelectionHorizontalAdapter()
            horizontalAdapter.setColorList(colors)
            binding.rvHorizontal.layoutManager = LinearLayoutManager(binding.rvHorizontal.context, LinearLayoutManager.HORIZONTAL, false)
            binding.rvHorizontal.adapter = horizontalAdapter
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val binding = SelectionHorizontalRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RecyclerViewHolder((binding))
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val colors = vertColorList[position]
        holder.add(colors)
    }

    override fun getItemCount(): Int {
        return vertColorList.size
    }

    fun addColorList(colors: List<Int>) {
        Log.e("VERT1", vertColorList.toString())
        vertColorList.add(colors)
        Log.e("VERT2", vertColorList.toString())
    }
}