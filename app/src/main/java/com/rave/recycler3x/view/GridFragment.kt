package com.rave.recycler3x.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.rave.recycler3x.databinding.GridFragmentBinding
import com.rave.recycler3x.grid_span
import com.rave.recycler3x.util.GridAdapter

class GridFragment: Fragment() {
    private var _binding: GridFragmentBinding? = null
    private val binding: GridFragmentBinding get() = _binding!!

    private val args by navArgs<GridFragmentArgs>()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = GridFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        val adapter = GridAdapter()
        rvGrid.layoutManager = GridLayoutManager(requireContext(), grid_span)
        adapter.setColorList(args.colors.toList())
        rvGrid.adapter = adapter
    }
}