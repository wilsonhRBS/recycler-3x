package com.rave.recycler3x.util

sealed class Resource {
    data class Success(val data: List<Int>): Resource()
    object Loading: Resource()
}

