package com.rave.recycler3x.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.recycler3x.horizontal_list_length
import com.rave.recycler3x.model.ReRepo
import com.rave.recycler3x.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SelectionViewModel: ViewModel() {
    private val repo = ReRepo

    private var _state: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val state: LiveData<Resource> get() = _state

    fun setLoading() {
        _state.value = Resource.Loading
    }

    fun fetchColors() = viewModelScope.launch(Dispatchers.Main) {
        _state.value = repo.fetchColorList(horizontal_list_length)
    }
}