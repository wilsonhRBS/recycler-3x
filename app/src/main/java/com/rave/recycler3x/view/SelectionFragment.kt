package com.rave.recycler3x.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.recycler3x.databinding.SelectionHomeFragmentBinding
import com.rave.recycler3x.util.Resource
import com.rave.recycler3x.util.SelectionVerticalAdapter
import com.rave.recycler3x.viewmodel.SelectionViewModel
import com.rave.recycler3x.viewmodel.SelectionViewModelFactory

class SelectionFragment: Fragment() {
    private var _binding: SelectionHomeFragmentBinding? = null
    private val binding: SelectionHomeFragmentBinding get() = _binding!!

    private lateinit var viewModel: SelectionViewModel
    private var colorList = mutableListOf<Int>()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = SelectionHomeFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, SelectionViewModelFactory())[SelectionViewModel::class.java]
        initViews()
    }

    private fun initViews() = with(binding) {
        val adapter = SelectionVerticalAdapter()

        viewModel.state.observe(viewLifecycleOwner) {
            when(it) {
                is Resource.Loading -> {
                    progress.isVisible = true
                    btnAdd.isEnabled = false
                    viewModel.fetchColors()
                }
                is Resource.Success -> {
                    progress.isVisible = false
                    btnAdd.isEnabled = true
                    binding.rvVertical.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                    adapter.addColorList(it.data)
                    colorList.addAll(it.data)
                    binding.rvVertical.adapter = adapter
                }
            }
        }

        btnAdd.setOnClickListener {
            viewModel.setLoading()
        }
        btnConsolidate.setOnClickListener {
            findNavController().navigate(SelectionFragmentDirections.actionSelectionFragmentToGridFragment(colorList.toIntArray()))
        }
    }
}