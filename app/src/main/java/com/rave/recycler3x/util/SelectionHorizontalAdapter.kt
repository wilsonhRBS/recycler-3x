package com.rave.recycler3x.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.recycler3x.databinding.SelectionListItemBinding

class SelectionHorizontalAdapter: RecyclerView.Adapter<SelectionHorizontalAdapter.RecyclerViewHolder>() {
    private lateinit var colorList: List<Int>

    class RecyclerViewHolder(private val binding: SelectionListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun add(color: Int) {
            binding.cardHorizontal.setCardBackgroundColor(color)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val binding = SelectionListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val color = colorList[position]
        holder.add(color)
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    fun setColorList(colors: List<Int>) {
        colorList = colors
    }
}