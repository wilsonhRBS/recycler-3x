package com.rave.recycler3x.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.recycler3x.databinding.SelectionListItemBinding

class GridAdapter: RecyclerView.Adapter<GridAdapter.GridViewHolder>() {
    private lateinit var colorList: List<Int>

    class GridViewHolder(private val binding: SelectionListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun add(color: Int) {
            binding.cardHorizontal.setCardBackgroundColor(color)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridViewHolder {
        val binding = SelectionListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GridViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        return holder.add(colorList[position])
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    fun setColorList(colors: List<Int>) {
        colorList = colors
    }
}